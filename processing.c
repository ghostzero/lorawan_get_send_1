/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

char const path[] = "/dev/shm/";

enum{
        MsgTypeReqNumFiles = 0,
        MsgTypeAnswNumFiles,
	MsgTypeReqFile,
	MsgTypeNameFile,
	MsgTypeOpenFile,
	MsgTypeReqData,
	MsgTypeRetry,
	MsgTypeData,
	MsgTypeEndFile,
	MsgTypeReqDelFile,
	MsgTypeSetSettings,
	MsgTypeAnswer,
	MsgTypeSwEvent,
	MsgTypeOnADC,
	MsgTypeOffADC,
	MsgTypeEnterLP,
	MsgTypeExitLP
};

enum{
	MsgAnswerOk = 0,
	MsgAnswerErr
};

#define HEADER_LEN 2 //size before data
typedef struct{
//	uint16_t devid;
	uint8_t index;
	uint8_t type;
	uint8_t data[62];
}lora_msg_t;

lora_msg_t lora_msg_tx, lora_msg_rx;
char out_str[32];
    
const int RxTime = 5000; //in ms
const uint8_t NumRetry = 10;



int LoraTxMsgWithRetry(int data_len){
    int rx_len = 0, retry = 0;
    int duration = -1;
    lora_msg_tx.index++;
//    lora_msg_tx.devid = 0;
    lora_tx_msg((uint8_t *) &lora_msg_tx, data_len + HEADER_LEN);
//    sx1276_7_8_LoRaEntryRx();
    
    while ((retry < NumRetry) && (rx_len == 0)) {
        duration = 0;
        while ((duration < RxTime) && (rx_len == 0)) {
            rx_len = lora_rx_msg(&lora_msg_rx);
            usleep(1000);
            duration++;
        }
        printf("duration(ms) = %d\r\n", duration);
        
        if(rx_len == 0){
            printf("Send Retry Msg\r\n");
            sprintf(out_str, "Retry %d", retry);
            return_tmp_result(out_str);
            lora_msg_tx.type = MsgTypeRetry;
            lora_tx_msg((uint8_t *) &lora_msg_tx, HEADER_LEN);
//            sx1276_7_8_LoRaEntryRx();
            retry++;
        }
    }
//    sx1276_7_8_Standby();
    return rx_len;
}

void GetFileList(){
    int rx_len, tx_len, run = 1, num_files = 0;
    char pattern[] = ".", *p, str[16];
    
    strcpy(lora_msg_tx.data, pattern);
    tx_len = strlen(pattern) + 1;
    
    while(run){
        lora_msg_tx.type = MsgTypeReqFile;
        rx_len = LoraTxMsgWithRetry(tx_len);

        if(rx_len > 0){
            if (lora_msg_rx.type == MsgTypeNameFile){
                p = lora_msg_rx.data + strlen(lora_msg_rx.data);
                *p++ = '\n';
                *p = 0;
           
                printf("FILE %s", lora_msg_rx.data);
                return_file_name(lora_msg_rx.data);//add filename in file
                num_files++;
                sprintf(str, "%d", num_files);
                return_tmp_result(str);
            }else{
                printf("Err: lora_msg_rx.type = %d, answ = %d\n", lora_msg_rx.type, lora_msg_rx.data[0]);
                return_tmp_result("OK");
                run = 0;
            }
        }else{
            printf("Err Timeout\n");
            return_tmp_result("TIMEOUT");
            run = 0;
        }
    }
    
    lora_msg_tx.type = MsgTypeOnADC;
    rx_len = LoraTxMsgWithRetry(0);
}

void GetFileFromLora(char *file_name){
    int rx_len, tx_len, end_file = 0;
    FILE *file;
    uint16_t sizecount = 0;
    char file_rasp[64];
    
    lora_msg_tx.type = MsgTypeOpenFile;
    tx_len = strlen(file_name);
    if(tx_len > sizeof(lora_msg_tx.data)) {
        tx_len = sizeof (lora_msg_tx.data);
    }
    strncpy(lora_msg_tx.data, file_name, tx_len);
    
    rx_len = LoraTxMsgWithRetry(tx_len);

    if(rx_len > 0){
        if (lora_msg_rx.type == MsgTypeAnswer){
            if(lora_msg_rx.data[0] == MsgAnswerOk){
                strcpy(file_rasp, path);
                strcat(file_rasp, file_name);
                
                file = open(file_rasp, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IROTH);
                sizecount = 0;
                end_file = 0;
                while(end_file == 0){
                    lora_msg_tx.type = MsgTypeReqData;
                    tx_len = 0;
                    rx_len = LoraTxMsgWithRetry(tx_len);
                    if(rx_len >= HEADER_LEN){
                        if(lora_msg_rx.type == MsgTypeData){
                            write(file, lora_msg_rx.data, rx_len - HEADER_LEN);
                            sizecount += rx_len - HEADER_LEN;
                            sprintf(out_str, "%d", sizecount);
                            printf("Receive byte : %s\n", out_str);
                            return_tmp_result(out_str);
                        }else if(lora_msg_rx.type == MsgTypeEndFile){
                            write(file, lora_msg_rx.data, rx_len - HEADER_LEN);
                            sizecount += rx_len - HEADER_LEN;
                            sprintf(out_str, "%d", sizecount);
                            printf("Receive byte : %s\n", out_str);                            
                            end_file = 1;
                            return_tmp_result("OK");
                        }else{
                            end_file = 1;
                            return_tmp_result("ERROR");
                        }
                    }else{ //timeout
                        end_file = 1;
                        return_tmp_result("ERROR");
                    }
                }
                printf("FILE %s, size = %d byte\n", file_name, sizecount);
                return_file_name(file_name);
                close(file);                
            }else{
                printf("Err Name: lora_msg_rx.type = %d, answ = %d\n", lora_msg_rx.type, lora_msg_rx.data[0]);
                return_tmp_result("ERROR");
            }
        }else{
            printf("Err: lora_msg_rx.type = %d, answ = %d\n", lora_msg_rx.type, lora_msg_rx.data[0]);
            return_tmp_result("ERROR");
        }
    }else{
        printf("Err Timeout\n");
        return_tmp_result("TIMEOUT");
    }
    
    lora_msg_tx.type = MsgTypeOnADC;
    rx_len = LoraTxMsgWithRetry(0);
}

void GetNumFiles(){
    int rx_len, num_files;
    char pattrn[] = ".";
    lora_msg_tx.type = MsgTypeReqNumFiles;
    strcpy(lora_msg_tx.data,pattrn);
    rx_len = LoraTxMsgWithRetry(strlen(pattrn) + 1);
    if(rx_len > 0){
        if (lora_msg_rx.type == MsgTypeAnswNumFiles){
            num_files = lora_msg_rx.data[0];
            sprintf(out_str, "%d", num_files);
            printf("Num files = %s\n", out_str);
            return_file_name(out_str);
            return_tmp_result("OK");
        }else{
            printf("Err: lora_msg_rx.type = %d, answ = %d\n", lora_msg_rx.type, lora_msg_rx.data[0]);
            return_tmp_result("ERROR");
        }
    }else{
        printf("Err Timeout\n");
        return_tmp_result("TIMEOUT");
    }   
}

void DelFile(char *filename){
    int rx_len, tx_len;
    lora_msg_tx.type = MsgTypeReqDelFile;
    tx_len = strlen(filename);
    if(tx_len > sizeof(lora_msg_tx.data)) {
        tx_len = sizeof (lora_msg_tx.data);
    }
    strncpy(lora_msg_tx.data, filename, tx_len);    
    
    rx_len = LoraTxMsgWithRetry(tx_len);
    if(rx_len > 0){
        if((lora_msg_rx.type == MsgTypeAnswer) 
        && (lora_msg_rx.data[0] == MsgAnswerOk)){
            return_tmp_result("OK");
            printf("Success\r\n");
        }else{
            return_tmp_result("ERROR");
            printf("Err: lora_msg_rx.type = %d, answ = %d\n", lora_msg_rx.type, lora_msg_rx.data[0]);
        } 
    }else{
        printf("Err Timeout\n");
        return_tmp_result("TIMEOUT");
    }   
}

void DoSwReq(){
    int rx_len;
    lora_msg_tx.type = MsgTypeSwEvent; 
    rx_len = LoraTxMsgWithRetry(0);
    if(rx_len > 0){
        if((lora_msg_rx.type == MsgTypeAnswer) 
        && (lora_msg_rx.data[0] == MsgAnswerOk)){
            return_tmp_result("OK");
        }else{
            return_tmp_result("ERROR");
        } 
    }else{
        printf("Err Timeout\n");
        return_tmp_result("TIMEOUT");
    }   
}

void SetTime(char *str){
    int rx_len, tx_len;
    uint32_t timestamp = atoi(str);
    lora_msg_tx.type = MsgTypeSetSettings;
    *(uint32_t*)lora_msg_tx.data = timestamp;
    tx_len = sizeof(timestamp);
    rx_len = LoraTxMsgWithRetry(tx_len);
    
    if(rx_len > 0){
        if((lora_msg_rx.type == MsgTypeAnswer) 
        && (lora_msg_rx.data[0] == MsgAnswerOk)){
            return_tmp_result("OK");
        }else{
            return_tmp_result("ERROR");
        } 
    }else{
        printf("Err Timeout\n");
        return_tmp_result("TIMEOUT");
    }    
}

void SetLPMode(char *mode){
    int rx_len;
    if(mode[0] == '0'){
        lora_msg_tx.type = MsgTypeEnterLP;
    }else{
        lora_msg_tx.type = MsgTypeExitLP;
    }
    rx_len = LoraTxMsgWithRetry(0);
    
    if(rx_len > 0){
        if((lora_msg_rx.type == MsgTypeAnswer) 
        && (lora_msg_rx.data[0] == MsgAnswerOk)){
            return_tmp_result("OK");
        }else{
            return_tmp_result("ERROR");
        } 
    }else{
        printf("Err Timeout\n");
        return_tmp_result("TIMEOUT");
    }    
}

void proc_while(void){
    int rx_len = 0, retry = 0, cnt = 0, i = 0;
    const char req[] = "PING", answ[] = "PONG";
/*    lora_msg_tx.index++;
    lora_msg_tx.devid = 0;
    lora_tx_msg((uint8_t *) &lora_msg_tx, data_len + HEADER_LEN);
 * */
    printf("Starting Ping Pong\r\n");
//    sx1276_7_8_LoRaEntryRx();
    
    while (1) {
        usleep(1000000);
        rx_len = lora_rx_msg(&lora_msg_rx);
        cnt++;
        if(cnt >= 100){
            cnt = 0;
//            printf("RSSI= %d\r\n", (int)(sx1276_7_8_ReadRSSI()) - 137);
        }
        
/*        if(rx_len){
            if(rx_len < sizeof(lora_msg_rx)){
                *(((uint8_t*)&lora_msg_rx) + rx_len) = 0;
            }
            printf("Resive msg: \"%s\"\r\n", (char *)&lora_msg_rx);
*/
            printf("Send Msg\r\n");
            for(i = 0; i < 10; i++)
                lora_tx_msg(answ, strlen(answ));
            
//            sx1276_7_8_LoRaEntryRx();            
//        }
        
    }
    sx1276_7_8_Standby();
}


