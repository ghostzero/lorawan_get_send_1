/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

enum{
    fn_processing = 0,
    fn_name,
    fn_snr,
    num_files          
};

char* const array_fn[] = {
    "/dev/shm/processing",
    "/dev/shm/file_name",
    "/dev/shm/snr"
};

enum{
    get_num_bin_files = 0,
    get_file_list,
    get_file,
    del_file,
    do_sw_event,
    set_settings,
    set_mode
};

#define USER_RIGTH (S_IRUSR | S_IRGRP | S_IWUSR | S_IROTH)

FILE *tmp_files[num_files];

void init_out_if(){
    int i;
/*    for(i = 0; i < num_files; i++){
        tmp_files[i] = open(array_fn[i], O_TRUNC | O_WRONLY | O_CREAT, S_IRUSR | S_IRGRP | S_IWUSR | S_IROTH);
    }
*/
    tmp_files[fn_name] = open(array_fn[fn_name], O_TRUNC | O_WRONLY | O_CREAT, USER_RIGTH);
}

void deinit_out_if(){
      int i;
    for(i = 0; i < num_files; i++){
        close(tmp_files[i]);
    }  
}

void return_file_name(char *msg){
    write(tmp_files[fn_name], msg, strlen(msg) + 1);
}

void return_tmp_result(char *msg){
//    lseek(tmp_files[fn_processing], 0, SEEK_SET);
    tmp_files[fn_processing] = open(array_fn[fn_processing], O_TRUNC | O_WRONLY | O_CREAT, USER_RIGTH);
    write(tmp_files[fn_processing], msg, strlen(msg) + 1);
    close(tmp_files[fn_processing]);
}

void return_snr_result(char *msg){
//    lseek(tmp_files[fn_processing], 0, SEEK_SET);
    tmp_files[fn_snr] = open(array_fn[fn_snr], O_TRUNC | O_WRONLY | O_CREAT, USER_RIGTH);
    write(tmp_files[fn_snr], msg, strlen(msg) + 1);
    close(tmp_files[fn_snr]);
}

void parce_input_console_cmd(char *cmd, char *arg){
    uint8_t int_cmd = atoi(cmd);
    printf("parce cmd %d; %s ->\r\n", int_cmd, arg);
    
    switch(int_cmd){
    case get_num_bin_files:
        GetNumFiles();
        break;
    case get_file_list:
        GetFileList();
        break;
    case get_file:
        if(arg)
            GetFileFromLora(arg);
        else{
            printf("no name\r\n");
            return_tmp_result("ERROR");
        }
        break;
    case del_file:
        if (arg)
            DelFile(arg);
        else {
            printf("no name\r\n");
            return_tmp_result("ERROR");
        }
        break;
//    case set_settings:
//        break;
    case do_sw_event:
        DoSwReq();
        break;     
    case set_settings:
        if (arg)
            SetTime(arg);
        else {
            printf("no arg\r\n");
            return_tmp_result("ERROR");
        }
        break;
    case set_mode:
        if (arg)
            SetLPMode(arg);
        else {
            printf("no arg\r\n");
            return_tmp_result("ERROR");
        }        
        break;       
    default:
        printf("unknown cmd = %d\r\n", int_cmd);
        return_tmp_result("ERROR");
        break;
    }
}