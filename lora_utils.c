/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <MQTTClient.h>


#define ADDRESS     "tcp://52.57.107.96:1883"
#define CLIENTID    "ExampleClientSub"
#define TOPIC       "application/1/node/00250c010000100c/tx"
#define TOPIC_GET   "application/1/node/00250c010000100c/rx"
#define PAYLOAD     "{\"confirmed\": true, \"fPort\": 10, \"data\":\"MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU2Nzg5\"}"
#define QOS         1
#define TIMEOUT     10000L

char global_msg[512];
int length_global_msg;


static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length) {

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = malloc(*output_length);
    if (encoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}


unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {

    if (decoding_table == NULL) build_decoding_table();

    if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}


void build_decoding_table() {

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}


void base64_cleanup() {
    free(decoding_table);
}

volatile MQTTClient_deliveryToken deliveredtoken;



volatile MQTTClient_deliveryToken deliveredtoken;



void delivered(void *context, MQTTClient_deliveryToken dt)
{
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    char* payloadptr;
    char template_message[512];
    char find_data [6];

    printf("Message arrived\n");
    printf("     topic: %s\n", topicName);
    printf("   message: ");

    payloadptr = message->payload;
    for(i=0; i<message->payloadlen; i++)
    {
        template_message[i]=*payloadptr++;
        putchar(template_message[i]);
        /*int count_find_data=0;
        if(i>=5){
            find_data[count_find_data]=template_message[i];
            count_find_data++;
            if(count_find_data==5){
            
            }
        }*/
        
        
    }
    putchar('\n');
    template_message[i+1]='\n';
//    char ukazatel_global_msg = *global_msg;
    char *index_template_message = template_message;
    char *index_result = strstr(template_message,"\"data\":\"");
    
    
    int position = index_result - index_template_message;
    int substringLength = strlen(index_template_message) - position;
    
    char message_data[substringLength-10];
    int i;
    for (i= 0 ; i<=substringLength-2; i++){
        message[i]=template_message[position+8+i];
    } 
    global_msg=message_data;
    length_global_msg=substringLength-10;
    
    printf("template_message:%s",template_message);
//    ukazatel_global_msg = base64_decode(payloadptr,message->payloadlen,&length_global_msg);
//    printf("Ukazatel_global_msg  inside msgarrvd:%s",ukazatel_global_msg);
//    length_global_msg = length_global_msg;
//    printf("Content global_msg by base64 %02X",global_msg);
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void connlost(void *context, char *cause)
{
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
}

//int main(int argc, char* argv[])
//{
//    printf("Test Vlad\n");
//    MQTTClient client;
//    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
//    int rc;
//    int ch;
//
//    MQTTClient_create(&client, ADDRESS, CLIENTID,
//        MQTTCLIENT_PERSISTENCE_NONE, NULL);
//    conn_opts.keepAliveInterval = 20;
//    conn_opts.cleansession = 1;
//    conn_opts.username = "anton";
//    conn_opts.password = "anton12345678";
//
//    MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);
//
//    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
//    {
//        printf("Failed to connect, return code %d\n", rc);
//        exit(EXIT_FAILURE);
//    }
//    printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
//           "Press Q<Enter> to quit\n\n", TOPIC, CLIENTID, QOS);
//    MQTTClient_subscribe(client, TOPIC, QOS);
//
//    do 
//    {
//        ch = getchar();
//    } while(ch!='Q' && ch != 'q');
//
//    MQTTClient_unsubscribe(client, TOPIC);
//    MQTTClient_disconnect(client, 10000);
//    MQTTClient_destroy(&client);
//    return rc;
//}









//void delivered(void *context, MQTTClient_deliveryToken dt)
//{
//    printf("Message with token value %d delivery confirmed\n", dt);
//    deliveredtoken = dt;
//}
//
//int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
//{
//    int i;
//    char* payloadptr;
//
//    printf("Message arrived\n");
//    printf("     topic: %s\n", topicName);
//    printf("   message: ");
//
//    payloadptr = message->payload;
//    for(i=0; i<message->payloadlen; i++)
//    {
//        putchar(*payloadptr++);
//    }
//    putchar('\n');
//    MQTTClient_freeMessage(&message);
//    MQTTClient_free(topicName);
//    return 1;
//}
//
//void connlost(void *context, char *cause)
//{
//    printf("\nConnection lost\n");
//    printf("     cause: %s\n", cause);
//}



void lora_tx_msg(uint8_t *msg, uint8_t len){
    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    int rc;
    
    
    size_t output_length;
    
//    printf("Message in hex:%s\n Message in base64:%s\n",msg,base64_encode(msg,strlen(msg),&output_length));
    int count_1=strlen(msg);
    for(int i=0;i<=count_1;i++){
        printf("%02X ",msg[i]);
    }
    char send_topic_1[256] ="{\"confirmed\": true, \"fPort\": 10, \"data\":\"";
    
//    printf("result_payload %s/n:",send_topic_1);
    
    char *send_topic_2 = base64_encode(msg,len,&output_length);
    
    char send_topic_3 [] ="\"}";
    
    strcat(send_topic_1,send_topic_2);
    
    strcat(send_topic_1,send_topic_3);
    
    printf("result_payload %s/n:",send_topic_1);
//    return;

//    const unsigned char check_string_hex="123456";
//    printf("Message in hex:%s\n Message in base:%s\n ",)

//    base64_encode
            
    MQTTClient_create(&client, ADDRESS, CLIENTID,
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
    conn_opts.username = "anton";
    conn_opts.password = "anton12345678";

    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
        exit(EXIT_FAILURE);
    }
    
    pubmsg.payload = send_topic_1;
    pubmsg.payloadlen = strlen(send_topic_1);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token);
    printf("Waiting for up to %d seconds for publication of %s\n"
            "on topic %s for client with ClientID: %s\n",
            (int)(TIMEOUT/1000), PAYLOAD, TOPIC, CLIENTID);
    rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
    printf("Message with delivery token %d delivered\n", token);
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    return rc;
    
//	uint8_t answ, bp;
//	answ = sx1276_7_8_LoRaTxPacket(msg, len, 1000);
//	if(answ){
//		//bp = 1;
//            printf("Timeout tx msg\r\n");
//	}

}

uint8_t lora_rx_msg(uint8_t *msg){
	uint8_t res, pack_size = 0, sz, i;
        
        printf("Test Vlad\n");
    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    int rc;
    int ch;

    MQTTClient_create(&client, ADDRESS, CLIENTID,
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
    conn_opts.username = "anton";
    conn_opts.password = "anton12345678";
    
    
    
    

    MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);

    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
        exit(EXIT_FAILURE);
    }
    printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
           "Press Q<Enter> to quit\n\n", TOPIC_GET, CLIENTID, QOS);
    MQTTClient_subscribe(client, TOPIC_GET, QOS);

//    do 
//    {
//        ch = getchar();
//    } while(ch!='Q' && ch != 'q');

    MQTTClient_unsubscribe(client, TOPIC_GET);
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    printf("Show rc:%d\n",rc);
    msg= global_msg;
//    return rc;
        
        
//	res = sx1276_7_8_LoRaRxCheckPacket();
//	if(res & LR_RX_DONE){//rxdone
//            printf("LR_RX_DONE ");
//            sz = sx1276_7_8_LoRaRxGetPacket(msg);
//            printf("%d: ", sz);
//            for(i = 0; i < sz; i++){
//                printf("%.2X ", msg[i]);
//            }
//            printf("\r\n");
//            
//            if(!(res & LR_PAYLOAD_CRC_ERROR)){ //crc err -> retransmit
////                    pack_size = sx1276_7_8_LoRaRxGetPacket(msg);
//                    pack_size = sz; //delete
//                    sx1276_7_8_Standby();
//            }
//            sx1276_7_8_LoRaClearIrq();
//            lora_read_snr();
//	}
    
	return length_global_msg;
}









